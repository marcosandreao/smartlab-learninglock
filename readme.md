## LRS plugin

# composer.json

"repositories": [
	{
        	"type":"vcs",
        	"url":"https://gitlab.com/marcosandreao/smartlab-learninglock.git"
    	
	}
	...
]

"require": {
	"smartlab/lrs":"dev-master"
	...
}

# Registrar provider
'Smartlab\Lrs\LrsServiceProvider'
