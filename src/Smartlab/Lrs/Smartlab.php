<?php

namespace Smartlab\Lrs;

use GuzzleHttp\Client;

class Smartlab implements ISmartlab {
	public function handlerSql($sql, $bindings, $times) {
		if ($this->startsWith ( $sql, "statements.batchInsert" )) {
			$sql = str_replace ( "statements.batchInsert([", "", $sql );
			$sql = str_replace ( "])", "", $sql );
			$obj = json_decode ( $sql );
			
			$statement = $obj->statement;
			
			$client = new Client ();
			$promise = $client->post ( "http://view.smartlab.dev/api/timeline/" . $statement->actor->account->name . "?action=" . $statement->verb->id . "&appid=" . $statement->object->id, [ 
					'headers' => [ 
							'Content-Type' => 'application/json' 
					] 
			] );
		}
	}
	public function startsWith($haystack, $needle, $case = true) {
		if ($case)
			return strpos ( $haystack, $needle, 0 ) === 0;
		
		return stripos ( $haystack, $needle, 0 ) === 0;
	}
}